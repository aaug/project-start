// Dependencies
var gulp = require('gulp'),
  babel = require('gulp-babel'),
  htmlImport = require('gulp-html-import'),
  sass = require('gulp-ruby-sass'),
  mmq = require('gulp-merge-media-queries'),
  cleanCSS = require('gulp-clean-css'),
  concat = require("gulp-concat"),
  uglify = require('gulp-uglify'),
  notify = require('gulp-notify'),
  imagemin = require('gulp-imagemin'),
  browserSync = require('browser-sync').create();


// Static server
gulp.task('browser-sync', () => {
  browserSync.init({
    server: {
      baseDir: "./CODE/dist/"
    }
  });
});

gulp.task('importhtml', () => {
  gulp.src('./CODE/src/*.html')
    .pipe(htmlImport('./CODE/src/html/'))
    .pipe(gulp.dest('./CODE/dist'))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: 'HTML task complete'
    }));
})

// Compile Our Sass
gulp.task('styles', () => {
  return sass('./CODE/src/sass/*.scss', {
      style: 'expanded'
    })
    .pipe(gulp.dest('./CODE/src/sass/mergeMQ/'))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: 'Styles task complete'
    }));
});

// Merge Media Queries (Requires 2 media queries)
gulp.task('mmq', () => {
  return gulp.src('./CODE/src/sass/mergeMQ/*.css')
    .pipe(mmq({
      log: true
    }))
    .pipe(gulp.dest('./CODE/src/css/'))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: 'Merge complete'
    }));
});

// Minify CSS
gulp.task('minify-css', () => {
  return gulp.src('./CODE/src/css/*.css')
    .pipe(cleanCSS({
      compatibility: 'ie8'
    }))
    .pipe(gulp.dest('./CODE/dist/css/'))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: 'CSS Minification complete'
    }));
});









gulp.task('babel', () => {
  return gulp.src('./CODE/src/es6/*.js')
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(gulp.dest('./CODE/src/js/'))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: 'JS Babel complete'
    }));
});

//Concat
gulp.task("concat", () => {
  return gulp.src(["./CODE/src/js/*.js"])
    .pipe(concat("app.js"))
    .pipe(gulp.dest("./CODE/src/js/toUglify/"))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: 'JS concat complete'
    }));
});

// Uglify JS
gulp.task('uglify-js', () => {
  return gulp.src('./CODE/src/js/toUglify/*.js')
    .pipe(uglify({
      mangle: false
    }))
    .pipe(gulp.dest('./CODE/dist/js/'))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: 'JS Uglification complete'
    }));
});









//Optimize images
gulp.task('minify-image', () => {
  return gulp.src('./CODE/src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./CODE/dist/img'))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: 'Image Minification complete'
    }));
});

// Watch Files For Changes
gulp.task('watch', () => {
  gulp.watch('./CODE/src/**/*.scss', ['styles'])
  gulp.watch('./CODE/src/**/*.html', ['importhtml'])
  gulp.watch('./CODE/src/**/*.css', ['mmq'])
  gulp.watch('./CODE/src/**/*.css', ['minify-css'])
  gulp.watch('./CODE/src/**/*.js', ['babel'])
  gulp.watch('./CODE/src/**/*.js', ['concat'])
  gulp.watch('./CODE/src/**/*.js', ['uglify-js'])
  //gulp.watch('src/**/*.html', ['minify-html'])
  gulp.watch(['./CODE/src/**/*.png', 'src/**/*.jpg'], ['minify-image'])
  gulp.watch(['./CODE/dist/**']).on('change', browserSync.reload);
});

// Run tasks
// gulp.task('default', ['browser-sync', 'importhtml', 'styles', 'mmq', 'minify-css', 'babel', 'concat', 'uglify-js', 'minify-image', 'watch']);
gulp.task('default', ['browser-sync', 'importhtml', 'styles', 'mmq', 'minify-css', 'babel', 'concat', 'uglify-js', 'minify-image', 'watch']);
